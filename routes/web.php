<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'NodeController@index')->name('index');

Auth::routes();


Route::middleware('auth')->group(function () {
    Route::get('/list', 'NodeController@list')->name('list');
    Route::get('/shop/{shop}', 'NodeController@shop')->name('shop.detail');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/month', 'NodeController@month');
    Route::get('/add', 'NodeController@add_revenue');

});

Route::get('/test/{title}', function ($title) {
    return view('coreui.' . $title);
});
Route::get('/test/base/{title}', function ($title) {
    return view('coreui.base.' . $title);
});
Route::get('/test/buttons/{title}', function ($title) {
    return view('coreui.buttons.' . $title);
});
Route::get('/test/icons/{title}', function ($title) {
    return view('coreui.icons.' . $title);
});
Route::get('/test/notifications/{title}', function ($title) {
    return view('coreui.notifications.' . $title);
});
