@extends ('layout')

@section('title', 'Главная')

@section('content')
    <h1>Монитор выполнения плана</h1>
    <div class="row">
        @foreach($mas as $ma)
        <div class="col-3">
             <div class="card card border-success mb-3" style="max-width: 20rem">
                <div class="card-header"><a href="{{ route('shop.detail', $ma['id']) }}">{{ $ma['name'] }}</a></div>
                <div class="card-body">
                    <h4 class="card-title"> {{ $ma['revenue'] }} рублей</h4>
                    <div class="progress">
                         <div class="progress-bar bg-success" role="progressbar" style="width: {{ $ma['percent'] }}%;" aria-valuenow="{{ $ma['revenue'] }}" aria-valuemin="0" aria-valuemax="100">{{ $ma['percent'] }}</div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>


@endsection