<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Icons-->
    <link href="{{ asset('vendor/icons/coreui-icons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/icons/flag-icon.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/icons/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/icons/simple-line-icons.min.css') }}" rel="stylesheet">
    <!-- Main styles for this application-->
    <link href="{{ asset('vendor/scss/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/scss/coreui.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/pace-progress/css/pace.min.css') }}" rel="stylesheet">

</head>
<body class="app flex-row align-items-center">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="clearfix">
                <h1 class="float-left display-3 mr-4">404</h1>
                <h4 class="pt-3">Oops! You're lost.</h4>
                <p class="text-muted">The page you are looking for was not found.</p>
            </div>
            <div class="input-prepend input-group">
                <div class="input-group-prepend">
              <span class="input-group-text">
                <i class="fa fa-search"></i>
              </span>
                </div>
                <input class="form-control" id="prependedInput" size="16" type="text" placeholder="What are you looking for?">
                <span class="input-group-append">
              <button class="btn btn-info" type="button">Search</button>
            </span>
            </div>
        </div>
    </div>
</div>

<!-- CoreUI and necessary plugins-->
<script src="{{ asset('vendor/js/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/js/popper.min.js') }}"></script>
<script src="{{ asset('vendor/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('vendor/pace-progress/pace.min.js') }}"></script>
<script src="{{ asset('vendor/js/perfect-scrollbar.min.js') }}"></script>
<script src="{{ asset('vendor/js/coreui.min.js') }}"></script>
<!-- Plugins and scripts required by this view-->
<script src="{{ asset('vendor/js/Chart.min.js') }}"></script>
<script src="{{ asset('vendor/js/custom-tooltips.min.js') }}"></script>

</body>
</html>
