<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Icons-->
    <link href="{{ asset('vendor/icons/coreui-icons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/icons/flag-icon.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/icons/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/icons/simple-line-icons.min.css') }}" rel="stylesheet">
    <!-- Main styles for this application-->
    <link href="{{ asset('vendor/scss/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/scss/coreui.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/pace-progress/css/pace.min.css') }}" rel="stylesheet">

</head>
<body class="app flex-row align-items-center">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-group">
                <div class="card p-4">
                    <div class="card-body">
                        <h1>Login</h1>
                        <p class="text-muted">Sign In to your account</p>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="icon-user"></i>
                    </span>
                            </div>
                            <input class="form-control" type="text" placeholder="Username">
                        </div>
                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="icon-lock"></i>
                    </span>
                            </div>
                            <input class="form-control" type="password" placeholder="Password">
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <button class="btn btn-primary px-4" type="button">Login</button>
                            </div>
                            <div class="col-6 text-right">
                                <button class="btn btn-link px-0" type="button">Forgot password?</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card text-white bg-primary py-5 d-md-down-none" style="width:44%">
                    <div class="card-body text-center">
                        <div>
                            <h2>Sign up</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <button class="btn btn-primary active mt-3" type="button">Register Now!</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- CoreUI and necessary plugins-->
<script src="{{ asset('vendor/js/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/js/popper.min.js') }}"></script>
<script src="{{ asset('vendor/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('vendor/pace-progress/pace.min.js') }}"></script>
<script src="{{ asset('vendor/js/perfect-scrollbar.min.js') }}"></script>
<script src="{{ asset('vendor/js/coreui.min.js') }}"></script>
<!-- Plugins and scripts required by this view-->
<script src="{{ asset('vendor/js/Chart.min.js') }}"></script>
<script src="{{ asset('vendor/js/custom-tooltips.min.js') }}"></script>

</body>
</html>

