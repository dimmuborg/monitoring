@extends ('layout')

@section('title', 'Главная')

@section('content')

<a href="/list"><h2>К списку магазинов</h2></a>

    <div class="row">
        @foreach($ar_kkm as $ars)
            <div class="col-3">
                <div class="card card border-success mb-3 card-padd" style="max-width: 20rem">
                    <div class="card-header">{{ $ars['name'] }}</div>
                    <div class="card-body">
                        <h4 class="card-title">{{ $ars ['revenue'] }} рублей</h4>
                        <div class="progress">
                            <div class="progress-bar bg-success" role="progressbar" style="width: {{ $ars['percent'] }}%;" aria-valuenow="" aria-valuemin="0" aria-valuemax="100">{{ $ars['percent'] }}</div>
                        </div>
                    </div>
                </div>
            </div>
{{--            <div class="col-3 shop">--}}
{{--                <div class="progress">--}}
{{--                    <div class="progress-bar bg-success" role="progressbar" style="width: {{ $ars['percent'] }}%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">{{ $ars['percent'] }}%</div>--}}
{{--                </div><h3>{{ $ars['name'] }}</h3>--}}
{{--            </div>--}}
        @endforeach
    </div>

@endsection