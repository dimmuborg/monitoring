
<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" href="/">
                    <i class="nav-icon icon-speedometer"></i> Dashboard
                    <span class="badge badge-primary">NEW</span>
                </a>
            </li>
            <li class="nav-title">Theme</li>
            <li class="nav-item">
                <a class="nav-link" href="/test/colors">
                    <i class="nav-icon icon-drop"></i> Colors</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/test/typography">
                    <i class="nav-icon icon-pencil"></i> Typography</a>
            </li>
            <li class="nav-title">Components</li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="nav-icon icon-puzzle"></i> Base</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="/test/base/breadcrumb">
                            <i class="nav-icon icon-puzzle"></i> Breadcrumb</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/test/base/cards">
                            <i class="nav-icon icon-puzzle"></i> Cards</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/test/base/carousel">
                            <i class="nav-icon icon-puzzle"></i> Carousel</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/test/base/collapse">
                            <i class="nav-icon icon-puzzle"></i> Collapse</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/test/base/forms">
                            <i class="nav-icon icon-puzzle"></i> Forms</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/test/base/jumbotron">
                            <i class="nav-icon icon-puzzle"></i> Jumbotron</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/test/base/list-group">
                            <i class="nav-icon icon-puzzle"></i> List group</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/test/base/navs">
                            <i class="nav-icon icon-puzzle"></i> Navs</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/test/base/pagination">
                            <i class="nav-icon icon-puzzle"></i> Pagination</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/test/base/popovers">
                            <i class="nav-icon icon-puzzle"></i> Popovers</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/test/base/progress">
                            <i class="nav-icon icon-puzzle"></i> Progress</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/test/base/scrollspy">
                            <i class="nav-icon icon-puzzle"></i> Scrollspy</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/test/base/switches">
                            <i class="nav-icon icon-puzzle"></i> Switches</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/test/base/tables">
                            <i class="nav-icon icon-puzzle"></i> Tables</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/test/base/tabs">
                            <i class="nav-icon icon-puzzle"></i> Tabs</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/test/base/tooltips">
                            <i class="nav-icon icon-puzzle"></i> Tooltips</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="nav-icon icon-cursor"></i> Buttons</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="/test/buttons/buttons">
                            <i class="nav-icon icon-cursor"></i> Buttons</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/test/buttons/button-group">
                            <i class="nav-icon icon-cursor"></i> Buttons Group</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/test/buttons/dropdowns">
                            <i class="nav-icon icon-cursor"></i> Dropdowns</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/test/buttons/brand-buttons">
                            <i class="nav-icon icon-cursor"></i> Brand Buttons</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/test/charts">
                    <i class="nav-icon icon-pie-chart"></i> Charts</a>
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="nav-icon icon-star"></i> Icons</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="/test/icons/coreui-icons">
                            <i class="nav-icon icon-star"></i> CoreUI Icons
                            <span class="badge badge-success">NEW</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/test/icons/flags">
                            <i class="nav-icon icon-star"></i> Flags</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/test/icons/font-awesome">
                            <i class="nav-icon icon-star"></i> Font Awesome
                            <span class="badge badge-secondary">4.7</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/test/icons/simple-line-icons">
                            <i class="nav-icon icon-star"></i> Simple Line Icons</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="nav-icon icon-bell"></i> Notifications</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="/test/notifications/alerts">
                            <i class="nav-icon icon-bell"></i> Alerts</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/test/notifications/badge">
                            <i class="nav-icon icon-bell"></i> Badge</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/test/notifications/modals">
                            <i class="nav-icon icon-bell"></i> Modals</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/test/widgets">
                    <i class="nav-icon icon-calculator"></i> Widgets
                    <span class="badge badge-primary">NEW</span>
                </a>
            </li>
            <li class="divider"></li>
            <li class="nav-title">Extras</li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="nav-icon icon-star"></i> Pages</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="/test/login" target="_top">
                            <i class="nav-icon icon-star"></i> Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/test/register" target="_top">
                            <i class="nav-icon icon-star"></i> Register</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/test/404" target="_top">
                            <i class="nav-icon icon-star"></i> Error 404</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/test/500" target="_top">
                            <i class="nav-icon icon-star"></i> Error 500</a>
                    </li>
                </ul>
            </li>

        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>

