<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kkm extends Model
{
    protected $fillable = ['id','shop_id', 'name', 'path'];
    public function shop() { return $this->belongsTo('App\Shop'); }
    public function revenue() { return $this->hasMany('App\Revenue'); }
}
