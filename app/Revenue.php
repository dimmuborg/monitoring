<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Revenue extends Model
{
    protected $fillable = ['id','kkm_id', 'day_sum'];
    public function kkm() { return $this->belongsTo('App\Kkm'); }
}

