<?php

namespace App\Http\Controllers;

use App\Revenue;
use Illuminate\Support\Carbon;
use App\Shop;

class NodeController extends Controller
{
    protected function index()
    {
        return view('welcome');
    }

    protected function list()
    {
        $shops = Shop::where('active', 1);

        foreach ($shops->get() as $key => $shop) {
            $mas[$key]['id'] = $shop->id;
            $mas[$key]['name'] = $shop->name;
            $mas[$key]['revenue'] = $this->month($shop->id, 'shop');
            $mas[$key]['percent'] = ($mas[$key]['revenue'] / 10000) * 100;
        }
        return view('list_shops', compact('mas'));
    }

    protected function shop($shop)
    {

        $kkmsFromShop = Shop::find($shop)->kkms()->get();


        foreach ($kkmsFromShop as $ids => $kkm) {
            $ar_kkm[$ids]['id'] = $kkm->id;
            $ar_kkm[$ids]['name'] = $kkm->name;
            $ar_kkm[$ids]['revenue'] = $this->month($kkm->id, 'kkm');
            $ar_kkm[$ids]['percent'] = ($ar_kkm[$ids]['revenue'] / 1000) * 100;
        }


        return view('list_kkm', compact('ar_kkm'));
    }

    protected function add_revenue()
    {
//        Revenue::create([
//            'kkm_id' => '2',
//            'day_sum' => '300'
//        ]);
        /*$shop = Shop::find($shopID);
        $nowRevenue = $shop->revenue;
        $shop->revenue = $nowRevenue + $daySum;*/
    }


    protected function month($id, $type)
    {
        $start = new Carbon('first day of this month');
        $end = new Carbon('last day of this month');
        $sum = 0;
        if ($type == 'kkm') {
            $sum = Revenue::where('kkm_id', $id)
                ->whereDay('created_at', '>=', $start)
                ->whereDay('created_at', '<=', $end)
                ->sum('day_sum');
        } elseif ($type == 'shop') {
            $kkms = Shop::find($id)->kkms()->get();
            foreach ($kkms as $kkm) {
                $getSums = Revenue::where('kkm_id', $kkm->id)
                    ->whereDay('created_at', '>=', $start)
                    ->whereDay('created_at', '<=', $end)
                    ->sum('day_sum');
                $sum = $sum + $getSums;
            }
        }

        return $sum;
    }
}
