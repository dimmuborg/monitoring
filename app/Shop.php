<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    protected $fillable = ['id', 'name'];

    public function kkms() { return $this->hasMany('App\Kkm'); }
}
